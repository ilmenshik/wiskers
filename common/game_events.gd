extends Node

signal game_dialog_initiated(dialog)
signal game_dialog_ended

signal player_healed
signal player_gained_health(value, reason)
signal player_gained_health_bonus(value, reason)
signal player_gained_damage(value, reason)
signal player_dead

signal enemy_interacted_with_player(body, enemy)


#
# The way to solve same tick signal conflict
#
func emit_dialog_initiated(dialog: Dialogue):
	call_deferred("emit_signal", "game_dialog_initiated", dialog)
	

func emit_dialog_finished():
	call_deferred("emit_signal", "game_dialog_ended")
