extends Resource

class_name RuntimeState

@export var skip_menu: bool = false
@export var current_gameplay_state: Enums.GameState
