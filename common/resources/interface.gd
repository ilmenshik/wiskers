extends Node

class Enemy:
	func take_damage():
		pass

	func get_damage():
		pass


class Friend:
	var _dialog: Dialogue
	

func _ready():
	var descendants = get_all_descendants(get_tree().current_scene)
	for d in descendants:
		check_node(d)
		
	get_tree().node_added.connect(check_node)
	print("[INTERFACE] Everything checked")


func get_all_descendants(node) -> Array:
	var children = [node]
	for child in node.get_children():
		children.append_array(get_all_descendants(child))
		
	return children

	
func check_node(node) -> void:
	if "implements" in node:
		var instance = node.implements.new()
		for property in instance.get_script().get_script_property_list():
			if property.name.begins_with("Built-in"):
				continue
			
			assert(
				property.name in node,
				"[INTERFACE] Error: " + node.name + " does not possess the property " + property.name + "."
			)

			assert(
				node.get(property.name) != null,
				"[INTERFACE] Error: " + node.name + " does not implemented properly, the property " + property.name + " is null."
			)

#			assert(
#				node.get(property.name).is_class(property.class_name),
#				"[INTERFACE] Error: Property " + property.name + " in " + node.name + " have wrong class " + property.class_name + "."
#			)

		for method in instance.get_script().get_script_method_list():
			assert(
				method.name in node,
				"[INTERFACE] Error: " + node.name + " does not possess the " + method.name + " method."
			)

func is_implements(implements_cls, object) -> bool:
	return "implements" in object and object.implements == implements_cls 

