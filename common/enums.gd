extends Node

enum CheatCodes {
	IMMORTAL,
	ENHANSERS,
	HEALTH,
}

enum GameState {
	FREE_WALK,
	IN_DIALOG,
	IN_CUT_SCENE,
}

enum PlayerState {
	IDLE,
	MOVING,
	JUMPING,
	FALLING,
	ATTACKING,
	DIALOG,
	HURT,
	DAMAGE,
}
