extends Resource

class_name Dialogue

@export var avatar_texture: Texture = preload("res://icon.svg")
@export var dialog_slides: Array[String]
