extends Node

const MOUSE_TOGGLE = "mouse_toggle"

const CONFIRM = "ui_accept"
const ESCAPE = "ui_cancel"
const BACK = "ui_text_backspace"

const MOVE_UP = "move_up"
const MOVE_DOWN = "move_down"
const MOVE_LEFT = "move_left"
const MOVE_RIGHT = "move_right"
const ACTION_JUMP = "action_jump"
const ACTION_USE = "action_use"
const ACTION_ATTACK = "action_attack"
const ACTION_DEBUG = "action_debug"
