extends Node

var mouse_state_free = Input.MOUSE_MODE_VISIBLE 
var mouse_state_capture = Input.MOUSE_MODE_VISIBLE # Input.MOUSE_MODE_CAPTURED
var input_sequence = ""
var _antiflood_tick = 0
var settings_scene = preload("res://screens/settings_menu/settings_menu.tscn")
var game_over_scene = preload("res://screens/gameover/game_over.tscn")

class Groups:
	static var player = "player"
	static var enemy = "enemy"
	static var collectable = "collectable"
	static var friend = "friend"

const __AllCheatCodes := {
	"iddqd": Enums.CheatCodes.IMMORTAL,
	"idkfa": Enums.CheatCodes.ENHANSERS,
	"idclip": Enums.CheatCodes.HEALTH,
}

signal cheat_code_activated(cheat_code)


func connect_cheats(callable):
	connect("cheat_code_activated", callable)

func _ready() -> void:
	Input.set_mouse_mode(mouse_state_capture)
	set_process_input(true)

func _input(event) -> void:
	if not event:
		return

	if Input.is_action_just_pressed(KeyCodes.ESCAPE):
		get_tree().quit(0)
	
#	if Input.is_action_just_pressed(KeyCodes.MOUSE_TOGGLE):
#		var mode = mouse_state_free
#		if Input.get_mouse_mode() == mouse_state_free:
#			mode = mouse_state_capture
#
#		Input.set_mouse_mode(mode)
	
	cheat_code(event)
	


func cheat_code(event):
	if event is InputEventKey and event.pressed and !event.echo:
		
		var entered_char = char(event.unicode).to_lower()
		if not entered_char:
			input_sequence = ""
			return

		input_sequence += entered_char
		for code in __AllCheatCodes:
			if input_sequence.ends_with(code):
				print("[GLOBAL] Cheat code activated: ", code)
				emit_signal("cheat_code_activated", __AllCheatCodes[code])
				input_sequence = ""


func action_start(next_world: PackedScene):
	get_tree().change_scene_to_packed(next_world)

	
func action_exit():
	get_tree().quit(0)


func action_settings():
	#current_world.visible = false
	get_tree().root.add_child(settings_scene.instantiate())

	
func game_over():
	get_tree().change_scene_to_packed(game_over_scene)



func antiflood(limit=5) -> bool:
		_antiflood_tick += 1
		if _antiflood_tick > limit:
				_antiflood_tick = 0
		if _antiflood_tick == 1:
				return true

		return false


func is_action_just_pressed(actions_of_interest: Array[String]) -> bool:
	var pressed_actions = []
	for action in actions_of_interest:
		if Input.is_action_just_pressed(action):
			return true
	
	return false

	
func is_action_pressed(actions_of_interest: Array[String]) -> bool:
	var pressed_actions = []
	for action in actions_of_interest:
		if Input.is_action_pressed(action):
			return true
	
	return false
