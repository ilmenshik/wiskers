extends Control

@export var _runtime_data: RuntimeState

@onready var ui_list  = {
	"default": %Default,
	"dialog": %Dialog,
}

func _ready():
	set_current_ui("default")
	GameEvents.game_dialog_initiated.connect(dialog_initiate)
	GameEvents.game_dialog_ended.connect(dialog_finish)


func set_current_ui(ui_name=null):
	for ui in ui_list:
		ui_list[ui].visible = (ui == ui_name)


func dialog_initiate(dialog: Dialogue):
	if dialog.dialog_slides.size():
		print("[HUD] Dialog initiated")
		_runtime_data.current_gameplay_state = Enums.GameState.IN_DIALOG
		ui_list["dialog"].dialog_initiate(dialog)
		set_current_ui("dialog")
	else:
		print("[HUD] Warning. Dialog skipped, because there is not slides")


func dialog_finish():
	print("[HUD] Dialog finished")
	set_current_ui("default")
	call_deferred("post_dialog")
	
func post_dialog():
	_runtime_data.current_gameplay_state = Enums.GameState.FREE_WALK

