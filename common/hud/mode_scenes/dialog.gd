extends MarginContainer

@export var _dialog_text: Label
@export var _dialog_avatar: TextureRect
@export var _current_dialog: Dialogue
@export var _runtime_data: RuntimeState

var _dialog_current_slide = 0

func _input(event):
	if Input.is_action_just_pressed(KeyCodes.CONFIRM) and _current_dialog:
		show_next_slide()


func show_next_slide() -> void:
	_dialog_current_slide += 1
	show_slide()

	
func show_slide() -> void:
	if _dialog_current_slide < _current_dialog.dialog_slides.size():
		_dialog_text.text = _current_dialog.dialog_slides[_dialog_current_slide]
	else:
		GameEvents.game_dialog_ended.emit()


func dialog_initiate(dialog: Dialogue):
	_current_dialog = dialog
	_dialog_current_slide = 0
	_dialog_avatar.texture = _current_dialog.avatar_texture
	_dialog_avatar.size.y = 58
	_dialog_avatar.size.x = 58
	show_slide()
	
func dialog_finish():
	_current_dialog = null
