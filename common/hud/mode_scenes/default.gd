extends Control

@export var health_container: Control
@export var current_health = 0
@export var current_max_health = 0
@export var _runtime_data: RuntimeState

const MAX_HEALTH = 9
const MIN_HEALTH = 0

var heart_alive = preload("res://common/hud/assets/sprites/heart-alive.png")
var heart_dead = preload("res://common/hud/assets/sprites/heart-dead.png")
var health_cheat_enabled = false
var heart_container = TextureRect
var _last_health = current_health
var _last_max_health = current_max_health


func _ready():
	Global.connect_cheats(_on_cheat_code_activated)
	GameEvents.player_gained_health_bonus.connect(add_max_health)
	GameEvents.player_gained_health.connect(add_health)
	GameEvents.player_gained_damage.connect(take_damage)
	redraw()


func _on_cheat_code_activated(cheat_code_value):
	if Enums.CheatCodes.HEALTH == cheat_code_value:
		health_cheat_enabled = true


func _process(delta):
	if _last_max_health != current_max_health or _last_health != current_health:
		redraw()


func adjust_limits():
	if current_max_health > MAX_HEALTH:
		current_max_health = MAX_HEALTH
	if current_max_health < MIN_HEALTH:
		current_max_health = MIN_HEALTH

	if current_health > MAX_HEALTH:
		current_health = MAX_HEALTH
	if current_health < MIN_HEALTH:
		current_health = MIN_HEALTH


func redraw():
	adjust_limits()

	for i in health_container.get_children():
		i.queue_free()
	
	for i in range(1, MAX_HEALTH + 1):
		var new_health = heart_container.new()
		new_health.add_to_group("health_items")
		if i <= current_max_health:
			if i <= current_health:
				new_health.texture = heart_alive
			else:
				new_health.texture = heart_dead
		else:
			new_health.texture = null
			
		health_container.add_child(new_health)


func add_max_health(value=1, reason="unknown") -> void:
	print("[HUD] Added max health: ", value, ", Reason: ", reason)
	current_max_health += value
	redraw()


func add_health(value, reason="unknown") -> void:
	print("[HUD] Added health: ", value, ", Reason: ", reason)
	current_health += value
	redraw()
	
func take_damage(value, reason="unknown") -> void:
	print("[HUD] called take damage")
	add_health(-value, reason)
