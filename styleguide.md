# Wiskers
## Naming
- files
  - case: snake_case
- methods:
  - case: snake_case
  - return boolean = starts with "is_" or "_is_"
  - should stay not more than 15 lines and 120 char length. split apart if needed.
- signals:
  - case: snake_case
  - starts with "on_" or "_on_"
- variables:
  - case: snake_case
  - use english words, the only available 1 letter variables: i,j,k - for loops indexes, f - while read file
- constants: UPPER_SNAKE_CASE
- global constants: CamelCaseCapital
- scenes:
  - case: snake_case
  - nodes in scene: CamelCaseCapital
  - hierarhy: role_folder -> group_folder (optional) -> scene_folder:
	- assets/
	  - sprites/
	  - sounds/
	- resources/
	  - scene_name.gd
	  - scene_name_shader.ext
	  - ...
	- scene_name.tscn
- states / animation states:
  - case: snake_case
- pivate/public methods
  - "_" for built-in methods
  - "__" private method and variables
  - without prefixes - everything else


## Definition order
Style guide: [gdscript_styleguide](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html)
- class_name
- extends
- signal
- exports
- var
- @onready
- built-in methods:
  - _init 
  - _ready
  - _input
  - _physics_update / _update
  - ...
- custom methods
- sub classes


## Programming
- Use signals instead of direct access between scenes (GameSignals singleton in particularly)
- Let nodes to know as less as possible about other nodes outside of the scene


## Where to find assets
https://itch.io/game-assets/free


## Setup
External editor:
```
idea --line {line} {file}
code {project} --goto {file}:{line}:{col}
```

External debug port: `6005`