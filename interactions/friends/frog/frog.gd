extends CharacterBody2D

@export var _dialog: Dialogue

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var implements = Interface.Friend
var _dialog_done = false

@onready var animation = $Sprite


func _physics_process(delta):
	if not is_on_floor():
		animation.play("Fall")
		velocity.y += gravity * delta
	else:
		if animation.name != "Idle":
			animation.play("Idle")

	move_and_slide()

func _on_interaction_area_body_entered(body):
	if body.is_in_group(Global.Groups.player):
		if not _dialog_done:
			_dialog_done = true
			GameEvents.game_dialog_initiated.emit(_dialog)


func _on_chase_area_body_entered(body):
	if body.name == "Player":
		var direction = (body.global_transform.origin - self.global_transform.origin).normalized().x
		animation.flip_h = direction > 0
		animation.stop()
		animation.frame = 0

func _on_chase_area_body_exited(body):
	if body.name == "Player":
		animation.play()
