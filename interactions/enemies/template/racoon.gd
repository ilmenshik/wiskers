extends CharacterBody2D

@export var speed = 50.0
@export var damage = 1

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var chase = false

@onready var animation = $Sprite

func get_damage():
	return damage

func _ready():
	add_to_group(Global.Groups.enemy)
	animation.play("Idle")


func _physics_process(delta):
	if not is_on_floor():
		velocity.y += gravity * delta

	move_and_slide()

func _on_interaction_area_body_entered(body):
	if body.is_in_group(Global.Groups.player):
		GameEvents.enemy_interacted_with_player.emit(body, self)


#func is_in_death() -> bool:
#	return FrogAnim.animation == "Death"


#func _on_PlayerDetection_body_entered(body):
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		chase = true

#func _on_PlayerDetection_body_shape_exited(body_rid, body, body_shape_index, local_shape_index):
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		chase = false

#func _on_PlayerDetectionDeath_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
#	if is_in_death():
#		return
#
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		var direction = (player.global_transform.origin - self.global_transform.origin).normalized().x
#		var tweenDisapear = get_tree().create_tween()
#		var selfBody = get_node("FrogCollision").get_parent()
#
#		FrogAnim.play("Death")
#		Global.play_new_sound(sndDeath, get_node("sounds"))
#		selfBody.collision_layer = 2
#		selfBody.collision_mask = 0
#		body.reward(REWARD_GOLD)
#		body.push_back(direction)
#		tweenDisapear.tween_property(self, "modulate:a", 0, 1)
#		tweenDisapear.tween_callback(self, "death_callback")

#func death_callback():
#	var cherryTemp = Cristal.instance()
#	cherryTemp.position = self.position - Vector2(0, 10)
#	get_node("../../collectables/timed_collectables").add_child(cherryTemp)
#	queue_free()


#func _on_PlayerCollision_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
#	if is_in_death():
#		return

#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		var direction = (body.global_transform.origin - self.global_transform.origin).normalized().x
#		body.damage(DAMAGE)
#		if is_instance_valid(body):
#			body.push_back(direction)
