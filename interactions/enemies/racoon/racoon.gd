extends CharacterBody2D

@export var speed = 20.0
@export var damage = 1
@export var range = 100.0

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var chase = false
var implements = Interface.Enemy
var movement_delta = 0
var starting_position = Vector2()
var going_right = true
var is_alive = true

@onready var animation = $Sprite


func _ready():
	starting_position = position
	

func _physics_process(delta):
	if not is_alive:
		return
		
	if going_right:
		velocity.x = speed
		if position.x > starting_position.x + range / 2:
			going_right = false
	else:
		velocity.x = -speed
		if position.x < starting_position.x - range / 2:
			going_right = true

	animation.flip_h = going_right
			
	if not is_on_floor():
		velocity.y += gravity * delta
	
	move_and_slide()


func get_damage():
	return damage


func take_damage():
	is_alive = false
	$BodyCollision.free()
	animation.play("death")
	animation.animation_finished.connect(queue_free)


func _on_chase_area_body_entered(body):
	pass

#func is_in_death() -> bool:
#	return FrogAnim.animation == "Death"


#func _on_PlayerDetection_body_entered(body):
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		chase = true

#func _on_PlayerDetection_body_shape_exited(body_rid, body, body_shape_index, local_shape_index):
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		chase = false

#func _on_PlayerDetectionDeath_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
#	if is_in_death():
#		return
#
#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		var direction = (player.global_transform.origin - self.global_transform.origin).normalized().x
#		var tweenDisapear = get_tree().create_tween()
#		var selfBody = get_node("FrogCollision").get_parent()
#
#		FrogAnim.play("Death")
#		Global.play_new_sound(sndDeath, get_node("sounds"))
#		selfBody.collision_layer = 2
#		selfBody.collision_mask = 0
#		body.reward(REWARD_GOLD)
#		body.push_back(direction)
#		tweenDisapear.tween_property(self, "modulate:a", 0, 1)
#		tweenDisapear.tween_callback(self, "death_callback")

#func death_callback():
#	var cherryTemp = Cristal.instance()
#	cherryTemp.position = self.position - Vector2(0, 10)
#	get_node("../../collectables/timed_collectables").add_child(cherryTemp)
#	queue_free()


#func _on_PlayerCollision_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
#	if is_in_death():
#		return

#	if not is_instance_valid(body):
#		return
#
#	if body.name == "Player":
#		var direction = (body.global_transform.origin - self.global_transform.origin).normalized().x
#		body.damage(DAMAGE)
#		if is_instance_valid(body):
#			body.push_back(direction)
