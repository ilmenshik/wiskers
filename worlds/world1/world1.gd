extends Node2D

@export var spawn: PackedScene

@onready var player = $Player

func _ready():
	pass

func _on_timer_timeout():
	#player.add_health(1)
	pass


func _on_spawn_timer_timeout():
	if spawn:
		var new_racoon = spawn.instantiate()
		new_racoon.global_transform = self.global_transform
		$RacoonSpawner.add_child(new_racoon)
