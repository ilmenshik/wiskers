class_name StateMachine
extends Node

############################################
#                                          #
#  State machine abstact class             #
#  Do not attach anywhere                  #
#  Do not override methods on inheritance  #
#                                          #
############################################

var state_output: Label
var __current_state: State
var __is_in_transition = false

func update():
	pass

func _ready():
	state_output = get_parent().find_child("StateOutput")
	for child in get_children():
		if child is State:
			child.puppet = get_parent()
			if __current_state == null:
				__current_state = child

	assert(__current_state, "There is no current state, possible because there is no children in this: " + self.name)
	__current_state.enter()

func _process(delta):
	update()
	
	if not __current_state:
		return
	
	if state_output:
		if __is_in_transition:
			state_output.text = ">>"
		else:
			state_output.text = __current_state.name
	
	
	if not __is_in_transition:
		#print("[FSM] Update: ", __current_state.name)
		__current_state.update(delta)
	
func _physics_process(delta):
	if not __current_state:
		return

	if not __is_in_transition:
		__current_state.physics_update(delta)
		

func _input(event):
	if not __current_state:
		return

	if not __is_in_transition:
		__current_state.input(event)


func transition_to(new_state: State):
	assert(is_ancestor_of(new_state), "New state should be ancestor of FSM")

	if not new_state or new_state == __current_state or __is_in_transition:
		return
		
	print("[FSM] ", get_parent().name, " transit ", __current_state.name, " -> ", new_state.name)
	__is_in_transition = true
	if __current_state:
		__current_state.exit()
		
	__current_state = new_state
	__current_state.enter()
	__is_in_transition = false
	
