class_name State
extends Node

var puppet

func enter():
	pass

	
func exit():
	pass

	
func update(delta: float):
	pass

	
func physics_update(delta: float):
	pass


func input(event):
	pass


func transition_to(new_state: State):
	get_parent().transition_to(new_state)
