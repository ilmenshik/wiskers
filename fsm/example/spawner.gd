extends Marker2D

@export var spawn_area: Area2D
@export var spawn_mob: PackedScene

func _input(event):
	if Input.is_action_just_pressed(KeyCodes.ACTION_USE):
		spawn()

func spawn():
	var new_mov = spawn_mob.instantiate()
	new_mov.global_position = get_random_position_in_rectangle()
	spawn_area.add_child(new_mov)

func get_random_position_in_rectangle() -> Vector2:
	var area_range = spawn_area.get_node("Range") as CollisionShape2D
	var rect_shape = area_range.shape as RectangleShape2D
	var extents = rect_shape.extents
	var area_position = area_range.global_position
	var random_x = randf_range(-extents.x, extents.x)
	var random_y = randf_range(-extents.y, extents.y)
	return area_position + Vector2(random_x, random_y)
