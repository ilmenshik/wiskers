extends CharacterBody2D

@export var _runtime_data: RuntimeEnemyState

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	if not is_on_floor():
		velocity.y += gravity

	move_and_slide()

func set_followee(body):
	if body is Player:
		_runtime_data.follow_body = body
	elif not body:
		_runtime_data.follow_body = null

func get_followee() -> Player:
	return _runtime_data.follow_body
