extends State

@onready var wander_area = $"../../WanderArea"
@onready var state_idle = $"../idle"

var follow_player: Player
var _rage = 0.0
var _max_rage = 5
var _go_rage = false


func enter():
	follow_player = puppet.get_followee()
#
#
func exit():
	puppet.set_followee(null)
	puppet.velocity = Vector2()

func convert_to_progress_bar_value(value: float, max_value: float) -> int:
	return int((value / max_value) * 100)
	
func update(_delta: float):
	if _go_rage:
		if _rage < _max_rage:
			_rage += _delta
		else:
			follow_player.damage(1)
			puppet.queue_free()
			
		$"../../ProgressBar".value = convert_to_progress_bar_value(_rage, _max_rage)
	
	
func get_shake_offset(rage: float) -> Vector2:
	return Vector2(randf_range(-rage, rage), randf_range(-rage, rage))


func physics_update(_delta: float):
	var player_pos = follow_player.global_position
	var self_pos = puppet.global_position
	var direction = (player_pos - self_pos).normalized()
	var distance = self_pos.distance_to(player_pos)
	
	var shake_offset = get_shake_offset(_rage)
	puppet.velocity = (direction + shake_offset) * 100
	
	



func _on_wander_area_body_exited(body):
	if not (body is Player):
		return
		
	get_parent().transition_to(state_idle)


func _on_rage_area_body_entered(body):
	if not (body is Player):
		return
		
	_go_rage = true


func _on_rage_area_body_exited(body):
	if not (body is Player):
		return
		
	_go_rage = false
	$"../../ProgressBar".value = 0
	_rage = 0
