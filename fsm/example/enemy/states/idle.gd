extends State

#func enter():
#	pass
#
#
#func exit():
#	pass
#
#
#func update(_delta: float):
#	pass
#
#
#func physics_update(_delta: float):
#	pass

@onready var state_follow = $"../follow"

func _on_wander_area_body_entered(body):
	if body is Player:
		puppet.set_followee(body)
		get_parent().transition_to(state_follow)
