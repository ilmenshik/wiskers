extends State

@export var state_idle: State
@export var state_fall: State
@export var state_attack: State


func enter():
	jump()

func physics_update(delta):
	if not puppet.is_on_floor():
		puppet.velocity.y += puppet.gravity * delta
		if puppet.velocity.y > 0:
			return transition_to(state_fall)
	else:
		puppet.reset_jump_counter()
		return transition_to(state_idle)
		
	if puppet.is_allowed_double_jump():
		jump()
		
	var direction = Input.get_axis(KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT)
	if direction:
		puppet.look_direction = direction
		puppet.velocity.x = direction * puppet.speed * puppet.get_sprint_modifier()
		puppet.move_and_slide()
	else:
		puppet.velocity.x = move_toward(puppet.velocity.x, 0, puppet.speed)
		puppet.move_and_slide()


func input(event):
	if Input.is_action_pressed(KeyCodes.ACTION_ATTACK):
		attack()
	elif Input.is_action_just_released(KeyCodes.ACTION_ATTACK):
		chill()


func jump():
	puppet.count_jump()
	puppet.velocity.y = -puppet.jump_velocity
	puppet.move_and_slide()


func attack():
	$"../../Attack".visible = true


func chill():
	$"../../Attack".visible = false

