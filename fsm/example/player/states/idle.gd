extends State

@export var state_walk: State
@export var state_fall: State
@export var state_jump: State


func physics_update(delta):
	if not puppet.is_on_floor():
		return transition_to(state_fall)

	if Global.is_action_pressed([KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT]):
		return transition_to(state_walk)

	if Global.is_action_just_pressed([KeyCodes.ACTION_JUMP]):
		return transition_to(state_jump)

