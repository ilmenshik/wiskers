extends State

@export var state_idle: State
@export var state_jump: State
@export var state_fall: State


func enter():
	$"../../Attack".visible = true


func exit():
	$"../../Attack".visible = false


func physics_update(delta):
	if not puppet.is_on_floor():
		puppet.velocity.y += puppet.gravity * delta
	
	if Input.is_action_just_released(KeyCodes.ACTION_ATTACK):
		return transition_to(state_fall)
			
			
	if puppet.is_on_floor():
		return transition_to(state_jump)
		
	var direction = Input.get_axis(KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT)
	if direction:
		puppet.velocity.x = direction * puppet.speed * puppet.get_sprint_modifier()
		puppet.move_and_slide()
	else:
		puppet.velocity.x = move_toward(puppet.velocity.x, 0, puppet.speed)
		puppet.move_and_slide()


#puppet.velocity.y = -puppet.jump_velocity
#puppet.move_and_slide()
