extends State

@export var _hurt_time = 2.0
@export var _hurt_step = 0.1
var _hurt_countdown = 0

func jump():
	puppet.count_jump()
	puppet.velocity.y = -puppet.jump_velocity / 3
	puppet.velocity.x = (puppet.jump_velocity / 2) * -puppet.look_direction

func enter():
	puppet.velocity = Vector2()
	jump()
	_hurt_countdown = _hurt_time

func update(delta):
	_hurt_countdown -= _hurt_step
	if Global.antiflood(1):
		puppet.visible = not puppet.visible
		
	puppet.move_and_slide()
	
	if _hurt_countdown <= 0:
		transition_to($"../idle")

