extends State

@export var state_idle: State


func enter():
	print("DialogEnter:PlayDialogAnimation(idle)")
	GameEvents.game_dialog_ended.connect(dialog_ended)


func update(delta):
	if Input.is_action_just_pressed(KeyCodes.ACTION_DEBUG):
		GameEvents.game_dialog_ended.emit()


func dialog_ended():
	return transition_to(state_idle)
