extends State

@export var state_idle: State
@export var state_jump: State


func physics_update(delta):
	if not puppet.is_on_floor():
		puppet.velocity.y += puppet.gravity * delta
		
	if Input.is_action_just_pressed(KeyCodes.ACTION_JUMP):
		return transition_to(state_jump)
		
	var direction = Input.get_axis(KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT)
	if direction:
		puppet.look_direction = direction
		puppet.velocity.x = direction * puppet.speed * puppet.get_sprint_modifier()
		puppet.move_and_slide()
	else:
		puppet.velocity.x = move_toward(puppet.velocity.x, 0, puppet.speed)
		puppet.move_and_slide()
		return transition_to(state_idle)
