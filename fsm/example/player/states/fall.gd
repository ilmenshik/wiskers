extends State

@export var state_idle: State
@export var state_jump: State
@export var state_attack: State


func physics_update(delta):
	if not puppet.is_on_floor():
		puppet.velocity.y += puppet.gravity * delta
	else:
		if Input.is_action_pressed(KeyCodes.ACTION_ATTACK):
			puppet.reset_jump_counter()
			return transition_to(state_jump)
		else:
			puppet.reset_jump_counter()
			return transition_to(state_idle)

	if puppet.is_allowed_double_jump():
		return transition_to(state_jump)
		
	var direction = Input.get_axis(KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT)
	if direction:
		puppet.look_direction = direction
		puppet.velocity.x = direction * puppet.speed * puppet.get_sprint_modifier()
		puppet.move_and_slide()
	else:
		puppet.velocity.x = move_toward(puppet.velocity.x, 0, puppet.speed)
		puppet.move_and_slide()


func input(event):
	if Input.is_action_pressed(KeyCodes.ACTION_ATTACK):
		attack()
	elif Input.is_action_just_released(KeyCodes.ACTION_ATTACK):
		chill()


func attack():
	$"../../Attack".visible = true


func chill():
	$"../../Attack".visible = false

