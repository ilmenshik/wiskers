extends StateMachine

@onready var state_dialog = $dialog

func update():
	if Input.is_action_just_pressed(KeyCodes.ACTION_DEBUG) and __current_state != state_dialog:
		call_deferred("transition_to", state_dialog)
