class_name Player
extends CharacterBody2D

@export var speed = 300.0
@export var jump_velocity = 600.0

@export var current_health = 3
@export var max_health = 9
@export var double_jump = 1
@export var jump_counter = 0
@export var player_state: StateMachine
@export var state_hurt: State

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var look_direction = 1



func _process(delta):
	var total_jumps = double_jump + 1
	$Label.text = "Jump: " + str(jump_counter) + " of " + str(total_jumps)


func is_allowed_double_jump() -> bool:
	var total_jumps = double_jump + 1
	return jump_counter < total_jumps and Input.is_action_just_pressed(KeyCodes.ACTION_JUMP)


func count_jump():
	jump_counter += 1


func reset_jump_counter():
	jump_counter = 0


func get_sprint_modifier():
	var modifier = 1
	if Input.is_action_pressed("ui_shift"):
		modifier = 2
	return modifier

func damage(value):
	player_state.transition_to(state_hurt)
