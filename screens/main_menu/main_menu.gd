extends Control

@export var load_world: PackedScene
@export var _runtime_data: RuntimeState
@export var skip_menu: bool
@onready var _menu_buttons = $Buttons
var _in_action = false
var _current_button = -1

func focus_first_button():
	_menu_buttons.get_children()[0].grab_focus()

func _ready():
	focus_first_button()
	if _runtime_data.skip_menu:
		_on_start_pressed()
	

func _on_start_pressed():
	print("[MAIN_MENU] Start pressed")
	if not _in_action and load_world:
		_in_action = true
		Global.action_start(load_world)
		_in_action = false


func _on_settings_pressed():
	print("[MAIN_MENU] Settings pressed")
	if not _in_action:
		_in_action = true
		Global.action_settings()
		_in_action = false


func _on_quit_pressed():
	print("[MAIN_MENU] Quit pressed")
	if not _in_action:
		_in_action = true
		Global.action_exit()
		_in_action = false
