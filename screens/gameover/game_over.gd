extends CanvasLayer

@export var load_world: PackedScene
@export var default_button: Button

func _ready():
	if default_button:
		default_button.grab_focus()
	

func _on_restart_pressed():
	Global.action_start(load_world)
