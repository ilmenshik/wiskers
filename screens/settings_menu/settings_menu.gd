extends Control


func _input(event):
	if Input.is_action_just_pressed(KeyCodes.CONFIRM):
		save_settings()
		close()
	elif Input.is_action_just_pressed(KeyCodes.BACK):
		close()


func close():
	get_parent().remove_child(self)
	

func save_settings():
	pass
