extends CharacterBody2D

@export var speed = 300.0
@export var jump_velocity = 400.0
@export var current_health = 3
@export var max_health = 9
@export var _runtime_data: RuntimeState
@export var hurt_immunity_frames = 20

@onready var animation = $Sprites
@onready var attack_sprite = $Sprites/Attack
@onready var state = $StateChart/ParallelState/Movement

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var player_state
var player_state_default = Enums.PlayerState.FALLING
var _hurt_immunity = 0
var _hurt_push_back_force = jump_velocity / 2
var _last_direction = 0


func _ready():
	Global.connect_cheats(_on_cheat_code_activated)
	init_health()


func _process(delta):
	if player_state:
		$CanvasLayer/LabelVelocityX.text = "VelocityX: " + str(round_to_dec(velocity.x, 4))
		$CanvasLayer/LabelVelocityY.text = "VelocityY: " + str(round_to_dec(velocity.y, 4))


func round_to_dec(num, digit):
	return round(num * pow(10.0, digit)) / pow(10.0, digit)


func _physics_process(delta):
	var is_moving = Input.is_action_pressed(KeyCodes.MOVE_LEFT) or Input.is_action_pressed(KeyCodes.MOVE_RIGHT)
	var is_jump = Input.is_action_just_pressed(KeyCodes.ACTION_JUMP)
	var is_attacking = Input.is_action_pressed(KeyCodes.ACTION_ATTACK)
	
	#if player_state:
	#	print(Enums.PlayerState.keys()[player_state])

	if _runtime_data.current_gameplay_state == Enums.GameState.IN_DIALOG:
		if self.player_state != Enums.PlayerState.DIALOG:
			animation.play("idle")
			_set_state(Enums.PlayerState.DIALOG)
		
	if self.player_state == Enums.PlayerState.IDLE:
		if is_moving:
			animation.play("walk")
			_set_state(Enums.PlayerState.MOVING)
		elif is_jump:
			jump()
			animation.play("jump")
			_set_state(Enums.PlayerState.JUMPING)
		else:
			add_gravity(delta)

	elif self.player_state == Enums.PlayerState.MOVING:
		handle_movement()
		if not is_moving:
			animation.play("idle")
			_set_state(Enums.PlayerState.IDLE)
		elif is_jump:
			jump()
			animation.play("jump")
			_set_state(Enums.PlayerState.JUMPING)
		elif not is_on_floor():
			animation.play("fall")
			_set_state(Enums.PlayerState.FALLING)
		else:
			add_gravity(delta)

	elif self.player_state == Enums.PlayerState.JUMPING:
		handle_movement()
		add_gravity(delta)
		if self.is_falling():
			animation.play("fall")
			self.player_state = Enums.PlayerState.FALLING
			_set_state(Enums.PlayerState.FALLING)			
		elif is_attacking:
			animation.play("attack")
			_set_state(Enums.PlayerState.ATTACKING)
		elif self.is_on_floor():
			animation.play("idle")
			_set_state(Enums.PlayerState.IDLE)

	elif self.player_state == Enums.PlayerState.FALLING:
		handle_movement()
		add_gravity(delta)
		if is_on_floor():
			if is_moving:
				animation.play("walk")
				_set_state(Enums.PlayerState.MOVING)
			else:
				animation.play("idle")
				_set_state(Enums.PlayerState.IDLE)
		elif is_attacking:
			animation.play("attack")
			_set_state(Enums.PlayerState.ATTACKING)

	elif self.player_state == Enums.PlayerState.ATTACKING:
		handle_movement()
		add_gravity(delta)
		if not is_attacking:
			if is_falling:
				animation.play("fall")
				_set_state(Enums.PlayerState.FALLING)
			else:
				animation.play("jump")
				_set_state(Enums.PlayerState.JUMPING)
		elif is_on_floor():
			jump()
			_set_state(Enums.PlayerState.JUMPING)

	elif self.player_state == Enums.PlayerState.DIALOG:
		if _runtime_data.current_gameplay_state != Enums.GameState.IN_DIALOG:
			_set_state()

	elif self.player_state == Enums.PlayerState.HURT:
		if _hurt_immunity < hurt_immunity_frames:
			_hurt_immunity += 1
		else:
			_hurt_immunity = 0
			push_back()
			_set_state()

	elif self.player_state == null:
		_set_state()

func _set_state(to_state = null):
	var states = Enums.PlayerState.keys()
	var cur_name = states[player_state] if player_state else "(null)"
	var new_name = states[to_state] if to_state else "(default)"

	if to_state == null:
		to_state = player_state_default

	print("[PlayerState] ", cur_name, " -> ", new_name)
	player_state = to_state
	$CanvasLayer/LabelState.text = "CurrentState: " + states[to_state]


func _input(event):
	if Input.is_action_just_pressed(KeyCodes.ACTION_USE):
		get_parent()._on_spawn_timer_timeout()


func jump():
	velocity.y = -jump_velocity
	move_and_slide()


func push_back():
	velocity.x = -velocity.x
	velocity.y = -jump_velocity
	move_and_slide()


func handle_movement():
	var direction = Input.get_axis(KeyCodes.MOVE_LEFT, KeyCodes.MOVE_RIGHT)
	if direction:
		_last_direction = direction
		velocity.x = direction * speed
		animation.flip_h = direction > 0
	else:
		_last_direction = 0
		velocity.x = move_toward(velocity.x, 0, speed)


func add_gravity(delta):
	velocity.y += gravity * delta
	move_and_slide()


func is_falling() -> bool:
	return not is_on_floor() and velocity.y > 0


func _interaction_area_collided(body):
	if Interface.is_implements(Interface.Enemy, body):
		if player_state == Enums.PlayerState.ATTACKING:
			body.take_damage()
			jump()
		elif player_state != Enums.PlayerState.HURT:
			take_damange(body.get_damage(), body)


func init_health():
	print("[CAT] Init max health ", max_health, ". Emit initial player_gained_health_bonus")
	GameEvents.player_gained_health_bonus.emit(max_health, "Initial")
	print("[CAT] Init current health ", current_health, ". Emit initial player_gained_health")
	GameEvents.player_gained_health.emit(current_health, "Initial")


func add_health(value):
	print("[CAT] add_health(", value, "). Emit player_gained_health")
	current_health += value
	GameEvents.player_gained_health.emit(value, "DebugHeal")


func take_damange(value, body):
	player_state = Enums.PlayerState.HURT
	print("[CAT] take_damange(", value, "). Emit player_gained_damage")
	GameEvents.player_gained_damage.emit(value, body.name)
	current_health -= value
	GameEvents.player_gained_damage.emit(value, body.name)
	if current_health <= 0:
		kill()


func kill():
	Global.game_over()


func _on_cheat_code_activated(code):
	if code == Enums.CheatCodes.HEALTH:
		add_health(1)
	elif code == Enums.CheatCodes.IMMORTAL:
		add_health(max_health)
